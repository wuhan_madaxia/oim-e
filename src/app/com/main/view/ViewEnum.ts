enum ViewEnum {
    PersonalView,
    ContactListPaneView,
    GroupListPaneView,
    MainView,
    MessageAllUnreadView,
    MessageListView,
    MessageAreaView,
    UserChatView,
    GroupChatView,
    GroupMemberListView,
}

export default ViewEnum;
